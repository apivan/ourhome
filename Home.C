﻿

#include <fstream>
#include <iostream>

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include <Wt/WAnchor.h>
#include <Wt/WApplication.h>
#include <Wt/WEnvironment.h>
#include <Wt/WLogger.h>
#include <Wt/WMenu.h>
#include <Wt/WPushButton.h>
#include <Wt/WStackedWidget.h>
#include <Wt/WTabWidget.h>
#include <Wt/WTable.h>
#include <Wt/WTableCell.h>
#include <Wt/WTemplate.h>
#include <Wt/WText.h>
#include <Wt/WViewWidget.h>
#include <Wt/WVBoxLayout.h>
#include <Wt/WHBoxLayout.h>
#include <Wt/WBootstrapTheme.h>
#include <Wt/WLinkedCssStyleSheet.h>
#include <Wt/WTimer.h>

#include "Home.h"
#include "view/BlogView.h"
#include "chat/SimpleChatServer.h"
#include "plot/NumericalCharts3D.h"
#include "map/Map.h"
#include "webasm/WebAsmWidget.h"

using namespace Wt;

static const std::string SRC_INTERNAL_PATH = "src";

Home::~Home() 
{
}

Home::Home(const WEnvironment& env,
	   Dbo::SqlConnectionPool& blogDb,
    SimpleChatServer& server)
  : WApplication(env),
    blogDb_(blogDb),
    homePage_(0),
    m_chatServer_(server)
{
    // To fix ?_= 
    // https://redmine.webtoolkit.eu/boards/2/topics/8304

  messageResourceBundle().use(appRoot() + "ourhome", false);


  useStyleSheet("css/home.css");

  setTitle("Our Home");
  

  auto theme = std::make_shared<Wt::WBootstrapTheme>();
  theme->setVersion(Wt::BootstrapVersion::v3);
  setTheme(theme);

  this->requireJQuery("jquery/jquery-3.3.1.min.js");

  this->require("js/bootstrap.min.js");
  this->require("webasm/EmscriptenSampleOgreCut.js");
  
  // load the default bootstrap3 (sub-)theme
  useStyleSheet("resources/themes/bootstrap/3/bootstrap-theme.min.css");
  useStyleSheet("resources/themes/bootstrap/3/bootstrap.min.css");
  // .. and an additional sylesheet for this application

  setLocale("");
  language_ = 0;

  addLanguage(Lang("en", "/", "en", "English"));
  addLanguage(Lang("th", "/th/", "th", "Thai"));

  init();

}

void Home::init()
{
  internalPathChanged().connect(this, &Home::setup);
  internalPathChanged().connect(this, &Home::setLanguageFromPath);

  setup();

  setLanguageFromPath();
}

void Home::setup()
{

  if (!homePage_) {
    root()->clear();

    createHome();

    setLanguageFromPath();
  }
}

void Home::updateBootstrapSettings()
{
    this->doJavaScript(R"[LIMIT](

$('.nav').affix({offset: {top: 90} }) 
$("#myCarousel").carousel({interval: 5000});

)[LIMIT]");
}

void Home::createHome()
{
  WTemplate *result = root()->addWidget(cpp14::make_unique<WTemplate>(tr("template")));
  homePage_ = result;

  auto languagesDiv = cpp14::make_unique<WContainerWidget>();
  languagesDiv->setId("top_languages");

  for (unsigned i = 0; i < languages.size(); ++i) {
    if (i != 0)
      languagesDiv->addWidget(cpp14::make_unique<WText>("- "));

    const Lang& l = languages[i];

    languagesDiv->addWidget(cpp14::make_unique<WAnchor>(WLink(LinkType::InternalPath, l.path_), l.longDescription_));
  }

  auto contents = cpp14::make_unique<WStackedWidget>();
  WAnimation fade(AnimationEffect::Fade, TimingFunction::EaseIn, 250);
  contents->setTransitionAnimation(fade);
  contents->setId("main_page");

  auto mainMenu = cpp14::make_unique<WMenu>(contents.get());
  mainMenu_ = mainMenu.get();
  mainMenu_->addItem
    (tr("overview"), introduction())->setPathComponent("");

  mainMenu_->addItem
    (tr("blog"), deferCreate(std::bind(&Home::blog, this)));

  auto contract = [this]() {
      auto page = cpp14::make_unique<WTemplate>(tr("home.contact"));
      Wt::WTimer::singleShot(std::chrono::milliseconds(0), [p = page.get()]() {
          p->bindWidget("map", CreateMap());
      });
      return page;
  };
  mainMenu_->addItem
  (tr("contact"), deferCreate(contract));
  
  // Make the menu be internal-path aware.  
  mainMenu_->setInternalPathEnabled("/");
  mainMenu_->setStyleClass("nav navbar-nav  navbar-right");

  result->bindWidget("languages", std::move(languagesDiv));
  result->bindWidget("menu", std::move(mainMenu));
  result->bindWidget("contents", std::move(contents));
  auto image = result->bindWidget("logo", std::make_unique<Wt::WImage>("icons/reusedlogo.png"));
  image->setWidth("80px");
  image->setHeight("80px");

  this->updateBootstrapSettings();

  this->doJavaScript(R"JS($('.navbar-nav>li>a').on('click', function(){
    $('.navbar-collapse').collapse('hide');
});)JS");

}

void Home::setLanguage(int index)
{
  if (homePage_) {
    const Lang& l = languages[index];

    setLocale(l.code_);

    std::string langPath = l.path_;
    mainMenu_->setInternalBasePath(langPath);
    BlogView *blog = dynamic_cast<BlogView *>(findWidget("blog"));
    if (blog)
      blog->setInternalBasePath(langPath + "blog/");

    language_ = index;
  }
}

void Home::setLanguageFromPath()
{
  std::string langPath = internalPathNextPart("/");

  if (langPath.empty())
    langPath = '/';
  else
    langPath = '/' + langPath + '/';

  int newLanguage = 0;

  for (unsigned i = 0; i < languages.size(); ++i) {
    if (languages[i].path_ == langPath) {
      newLanguage = i;
      break;
    }
  }

  if (newLanguage != language_)
    setLanguage(newLanguage);
}

std::unique_ptr<WWidget> Home::introduction()
{
  auto page = cpp14::make_unique<WTemplate>(tr("home.overview"));
  page->bindWidget("matlab", NumChart3d());
  page->bindWidget("ocean", WebAsmWidget());
  return page;
}

std::unique_ptr<WWidget> Home::blog()
{
  const Lang& l = languages[language_];
  std::string langPath = l.path_;

  auto container = cpp14::make_unique<WTemplate>(tr("blogViewContainer"));
  auto blog = container->bindWidget("blogview", cpp14::make_unique<BlogView>(langPath + "blog/",
      blogDb_, "/wt/blog/feed/"));

  this->doJavaScript(R"RXX(loadScript("/chat.js?div=chat", null))RXX");

  blog->setObjectName("blog");


  blog->userChanged().connect([this](Wt::WString userName) {

    if (userName.empty())
    {
        userName = this->m_chatServer_.suggestGuest();
    }
    auto string = (boost::format(R"RXX(
if (window.chat && window.chat.emit) {
    try { 
      window.chat.emit(window.chat, 'login', "%1%");
    } catch (e) {
    }
}

)RXX") % userName.jsStringLiteral()).str();

    WApplication::instance()->doJavaScript(string);

  });


  return std::move(container);
}

std::string Home::href(const std::string& url, const std::string& description)
{
  return "<a href=\"" + url + "\" target=\"_blank\">" + description + "</a>";
}

WString Home::tr(const char *key)
{
  return WString::tr(key);
}

std::unique_ptr<WWidget> Home::wrapView(std::unique_ptr<WWidget>(Home::*createWidget)())
{
  return makeStaticModel(std::bind(createWidget, this));
}
