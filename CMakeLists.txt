cmake_minimum_required (VERSION 3.11.4)
project (ourhome)


set(Boost_USE_STATIC_LIBS        ON) # only find static libs
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
set(Boost_NO_SYSTEM_PATHS       ON)

ADD_DEFINITIONS(-DHPDF_DLL)

if(WIN32)
set(BOOST_ROOT     "C:/local/boost_1_68_0")
set(WTROOT "C:/local/Wt 4.0.4 msvs2017 x64")
endif()

if(UNIX )
set(BOOST_ROOT     "/usr/local/")
set(WTROOT "/usr/local") 
endif()

# Manually find package WT. wt-config.cmake doesn't seem to work.
set(WT_COMPONENT_NAME wt wthttp wttest wtdbo wtdbosqlite3)
set(WT_LIBDIR   ${WTROOT}/lib)
set(WT_BINDIR   ${WTROOT}/bin)
set(WT_INCLUDE    ${WTROOT}/include)
if(UNIX)
    foreach(component ${WT_COMPONENT_NAME}) 
        set(WT_LIBFILES ${WT_LIBFILES} ${WT_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${component}.so)
    endforeach()
elseif(WIN32)
    foreach(component ${WT_COMPONENT_NAME}) 
        set(WT_LIBFILES ${WT_LIBFILES} debug ${WT_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${component}d.lib)
        set(WT_LIBFILES ${WT_LIBFILES} optimized ${WT_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${component}.lib)
    endforeach()
endif()


find_package(Boost COMPONENTS date_time filesystem system thread regex )


add_executable(ourhome  ${SRC})
target_sources(ourhome PRIVATE main.C
	Home.C
	FileItem.C
	SourceView.C
	ExampleSourceViewer.C
	blog/BlogRSSFeed.C
	blog/view/BlogLoginWidget.C
    blog/view/BlogView.C
	blog/view/CommentView.C
	blog/view/PostView.C
  	blog/view/EditUsers.C
	blog/model/BlogSession.C
	blog/model/BlogUserDatabase.C
	blog/model/Comment.C
	blog/model/Post.C
	blog/model/User.C
	blog/model/Tag.C
	blog/model/Token.C
	blog/asciidoc/asciidoc.C
	chat/PopupChatWidget.C
	chat/SimpleChatServer.C
	chat/SimpleChatWidget.C
	plot/DataModels.C
	plot/NumericalCharts3D.C
	map/Map.C
	webasm/WebAsmWidget.C)



if(UNIX)
    target_link_libraries(ourhome crypt)
endif()

target_link_libraries(ourhome ${Boost_LIBRARIES} ${WT_LIBFILES})


add_custom_command(TARGET ourhome POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/runFiles $<TARGET_FILE_DIR:ourhome>
)

add_custom_target(copyrunfiles
    ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/runFiles $<TARGET_FILE_DIR:ourhome>
)

if(WIN32) # copy user setting for visual studio
	add_custom_command(TARGET ourhome POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/ourhome.vcxproj.user $<TARGET_FILE_DIR:ourhome>/../ourhome.vcxproj.user
	)
endif()

target_include_directories(ourhome
 PUBLIC ${WT_INCLUDE}  ${Boost_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR}/blog)

set_property(TARGET ourhome PROPERTY CXX_STANDARD 14)


if(WIN32)
    SET(CMAKE_INSTALL_PREFIX "C:/DevBuild/ourhome") 
endif()

if(UNIX )
    SET(CMAKE_INSTALL_PREFIX  "/usr/local/DevBuild")
endif()

INSTALL(
    TARGETS ourhome
    RUNTIME
    DESTINATION programs
    COMPONENT   applications
)

INSTALL(
    DIRECTORY  ${CMAKE_SOURCE_DIR}/runFiles/
    DESTINATION programs
    COMPONENT applications
)
    
foreach(component ${WT_COMPONENT_NAME}) 
    set(WT_LIBDLLS ${WT_LIBDLLS} ${WT_BINDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}${component}.dll)
endforeach()

INSTALL(FILES ${WT_LIBDLLS} 
	    ${WT_BINDIR}/ssleay32.dll ${WT_BINDIR}/libhpdf.dll
		${WT_BINDIR}/libeay32.dll ${WT_BINDIR}/libpng14.dll 
		${WT_BINDIR}/zlib.dll
        DESTINATION programs
        COMPONENT applications)
            
# Dont pack vc++ redist 
SET(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_SKIP TRUE)
    
INCLUDE(InstallRequiredSystemLibraries)
    
INSTALL(PROGRAMS    ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS}
        DESTINATION programs
        COMPONENT   applications)


# ================================================
# Install files (Windows only)
# ================================================
IF(WIN32)

    SET(CPACK_GENERATOR ZIP)
    SET(CPACK_PACKAGE_VERSION_MAJOR 0)
    SET(CPACK_PACKAGE_VERSION_MINOR 2)
    SET(CPACK_PACKAGE_VERSION_PATCH 9)
	SET(CPACK_PACKAGE_NAME  ${PROJECT_NAME}-portable)
    INCLUDE(CPack)
ENDIF()
 


