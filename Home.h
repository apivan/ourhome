
#ifndef HOME_H_
#define HOME_H_

#include <Wt/WApplication.h>
#include <Wt/WContainerWidget.h>

namespace Wt {
    class WMenu;
    class WStackedWidget;
    class WTabWidget;
    class WTreeNode;
    class WTable;
}

class SimpleChatServer;

using namespace Wt;

struct Lang {
    Lang(const std::string& code, const std::string& path,
        const std::string& shortDescription,
        const std::string& longDescription) :
        code_(code),
        path_(path),
        shortDescription_(shortDescription),
        longDescription_(longDescription) {
    }

    std::string code_, path_, shortDescription_, longDescription_;
};

/*
 * A utility container widget which defers creation of its single
 * child widget until the container is loaded (which is done on-demand
 * by a WMenu). The constructor takes the create function for the
 * widget as a parameter.
 *
 * We use this to defer widget creation until needed.
 */
template <typename Function>
class DeferredWidget : public WContainerWidget
{
public:
    DeferredWidget(Function&& f)
        : f_(std::forward<Function>(f)) { }

private:
    void load() {
        WContainerWidget::load();
        if (count() == 0)
            addWidget(f_());
    }

    Function f_;
};

template <typename Function>
std::unique_ptr<DeferredWidget<Function>> deferCreate(Function&& f)
{
    return cpp14::make_unique<DeferredWidget<Function>>(std::forward<Function>(f));
}


class Home : public WApplication
{
public:

    Home(const WEnvironment& env, Dbo::SqlConnectionPool& blogDb
    , SimpleChatServer& server);


    virtual ~Home();



private:

    void createHome();

    std::unique_ptr<WWidget> introduction();
    std::unique_ptr<WWidget> blog();
    std::string filePrefix() const { return "wt-"; }
    void init();
    void addLanguage(const Lang& l) { languages.push_back(l); }
    WString tr(const char *key);
    std::string href(const std::string& url, const std::string& description);
    std::unique_ptr<WWidget> wrapView(std::unique_ptr<WWidget>(Home::*createFunction)());

    void setLanguage(int language);
    void setLanguageFromPath();
    void setup();

    void updateBootstrapSettings();

    WMenu *mainMenu_ = nullptr;

    int language_ = -1 ;

    Dbo::SqlConnectionPool& blogDb_;

    WWidget *homePage_ = nullptr;

    WStackedWidget *contents_ = nullptr;

    std::vector<Lang> languages;

    SimpleChatServer& m_chatServer_;

};




#endif // HOME_H_
