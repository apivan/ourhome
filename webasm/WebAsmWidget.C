#include <Wt/WApplication.h>
#include <Wt/WCssDecorationStyle.h>
#include <Wt/WBorder.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WImage.h>
#include <Wt/WTemplate.h>
#include <Wt/WResource.h>
#include <Wt/WFileResource.h>
#include <Wt/WServer.h>
#include <Wt/WTimer.h>
#include <Wt/WHBoxLayout.h>

#include "WebAsmWidget.h"


const auto emscriptenHtml = R"__[]__(
    <div class="spinner" id='spinner'></div>
    <div class="emscripten" id="status">Downloading...</div>
    <div class="emscripten">
      <progress value="0" max="100" id="progress" hidden=1></progress>
    </div>
    <canvas class="emscripten" id="canvas" oncontextmenu="event.preventDefault()" ></canvas>
    <caption>A webassembly program. If you are using mobile phone, please use "Request destop site" to reveal all the view.</caption>
<style>
@media screen   {
  #canvas {
    max-width: 1122px;
    min-width: 1122px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
@media screen and (max-width: 1200px)  {
  #canvas {
    max-width: 922px;
    min-width: 922px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
@media screen and (max-width: 992px)   {
  #canvas {
    max-width: 702px;
    min-width: 702px;
    user-zoom: fixed;
    min-zoom: 1.0;
    max-zoom: 1.0;
    zoom: 1.0;
  }
}
</style>
)__[]__";


std::unique_ptr<Wt::WWidget> WebAsmWidget()
{
    using namespace Wt;
    auto container = cpp14::make_unique<WTemplate>();

    Wt::WApplication::instance()->useStyleSheet("/css/webasm.css");

    container->setTemplateText(emscriptenHtml, Wt::TextFormat::UnsafeXHTML);
    container->addStyleClass("center-text");
    container->setHeight(1000);
    container->doJavaScript(R"__[]__(
var statusElement = document.getElementById('status');
var progressElement = document.getElementById('progress');
var spinnerElement = document.getElementById('spinner');

var Module = {
  preRun: [],
  postRun: [],
  print: (function() {
   // var element = document.getElementById('output');
   // if (element) element.value = ''; // clear browser cache
    return function(text) {
      if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
      // These replacements are necessary if you render to raw HTML
      //text = text.replace(/&/g, "&amp;");
      //text = text.replace(/</g, "&lt;");
      //text = text.replace(/>/g, "&gt;");
      //text = text.replace('\n', '<br>', 'g');
      console.log(text);
     // if (element) {
    //    element.value += text + "\n";
    //    element.scrollTop = element.scrollHeight; // focus on bottom*/
    //  }
    };
  })(),
  printErr: function(text) {
    if (arguments.length > 1) text = Array.prototype.slice.call(arguments).join(' ');
    if (0) { // XXX disabled for safety typeof dump == 'function') {
      dump(text + '\n'); // fast, straight to the real console
    } else {
      console.error(text);
    }
  },
  canvas: (function() {
    var canvas = document.getElementById('canvas');

    // As a default initial behavior, pop up an alert when webgl context is lost. To make your
    // application robust, you may want to override this behavior before shipping!
    // See http://www.khronos.org/registry/webgl/specs/latest/1.0/#5.15.2
    canvas.addEventListener("webglcontextlost", function(e) { alert('WebGL context lost. You will need to reload the page.'); e.preventDefault(); }, false);

    return canvas;
  })(),
  setStatus: function(text) {
    if (!Module.setStatus.last) Module.setStatus.last = { time: Date.now(), text: '' };
    if (text === Module.setStatus.last.text) return;
    var m = text.match(/([^(]+)\((\d+(\.\d+)?)\/(\d+)\)/);
    var now = Date.now();
    if (m && now - Module.setStatus.last.time < 30) return; // if this is a progress update, skip it if too soon
    Module.setStatus.last.time = now;
    Module.setStatus.last.text = text;
    if (m) {
      text = m[1];
      progressElement.value = parseInt(m[2])*100;
      progressElement.max = parseInt(m[4])*100;
      progressElement.hidden = false;
      spinnerElement.hidden = false;
    } else {
      progressElement.value = null;
      progressElement.max = null;
      progressElement.hidden = true;
      if (!text) spinnerElement.style.display = 'none';
    }
    statusElement.innerHTML = text;
  },
  totalDependencies: 0,
  monitorRunDependencies: function(left) {
    this.totalDependencies = Math.max(this.totalDependencies, left);
    Module.setStatus(left ? 'Preparing... (' + (this.totalDependencies-left) + '/' + this.totalDependencies + ')' : 'All downloads complete.');
  }
};
Module.setStatus('Downloading...');
window.onerror = function(event) {
  // TODO: do not warn on ok events like simulating an infinite loop or exitStatus
  Module.setStatus('Exception thrown, see JavaScript console');
  spinnerElement.style.display = 'none';
  Module.setStatus = function(text) {
    if (text) Module.printErr('[post-exception status] ' + text);
  };
};

InitModule(Module);
)__[]__");

    return container;
}
